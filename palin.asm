ORG 1000H                          ;Campo para variables inicializadas
MSJ DB "Ingrese una palabra "
    DB "(Esc para terminar):"
RETRO DB "Si es un palindromo"
NEG_RETRO DB "No es palindromo"
CHARS_MSJ DB 40
CHARS_RETRO DB 19
NEG_CHARS_RETRO DB 16
CONTADOR DB 0
DETENER DB 27                      ;Representa en decimal 'Esc', usamos para detener proceso
ESPACIO DB 32
ORG 3000H                          ;Campo para variables declaradas
ARR_TEXTO DB 1 DUP(0)              ;Arreglo "vacio"
ORG 2000H                          ;Campo para algoritmo logico
MOV bx, OFFSET MSJ
MOV al, CHARS_MSJ
INT 7
MOV bx, OFFSET ARR_TEXTO
RECIBIR_ENTRADA: INT 6
MOV dl, [bx]
MOV al, 1
INT 7
CMP dl, ESPACIO                    ;Evitamos los espacios para poder validar frases completas
JZ RECIBIR_ENTRADA
CMP dl, DETENER                    ;Detectamos si quiere detener ingreso de palabras
JZ ALMACENAR_TEXTO
INC CONTADOR                       ;Por cada caracter, incrementamos contador
INC bx
JMP RECIBIR_ENTRADA
ALMACENAR_TEXTO: MOV ax, bx        ;Guardamos contenido de bx (ARR_TEXTO) en ax
DEC ax
MOV bx, OFFSET ARR_TEXTO           ;Guardamos referencia de ARR_TEXTO en bx
VALIDAR_ENTRADA: MOV dh, [bx]
PUSH bx
MOV bx, ax
MOV dl, [bx]
CMP dh, dl
JNZ NO_PALIN                       ;Si existe un valor valor distinto en dh, dl, no es palindromo
DEC bx                             ;e interrumpimos el flujo
MOV ax, bx
POP bx
INC bx
DEC CONTADOR                       ;Decrementamos contador por cada caracter procesado
JZ SI_PALIN                        ;Si contador llega a 0, tenemos un palindromo
JMP VALIDAR_ENTRADA
NO_PALIN: MOV bx, OFFSET NEG_RETRO
MOV al, NEG_CHARS_RETRO
INT 7
JMP FIN
SI_PALIN: MOV bx, OFFSET RETRO
MOV al, CHARS_RETRO
INT 7
FIN: HLT
END
