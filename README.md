# palin_asm

palin_asm is a small tidbit of code in Assembly to check if a word is a palindrome or not

## Requirements

This has only been verified to work in MSX88 emulator

## Usage

```bash
ASM88 palin_asm.asm
LINK88 PALIN_ASM.0
MSX88
```

## Contributing
Why on Earth would you contribute to this? But if you do, please open an issue :)

## License
[MIT](https://choosealicense.com/licenses/mit/)
